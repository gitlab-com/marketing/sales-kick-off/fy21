# FY21 Sales Kick Off

[**FY21 Sales Kick Off Planning doc**](https://docs.google.com/document/d/16PsAlyzq2seY2I10JAK9ANm6fPxtVJ_fOZpeiBhGWJc/edit)

[**Draft agenda**](https://docs.google.com/spreadsheets/d/1bN56qrOHrJT6FbvyF7Kz0nC6DBXDLxLYyw8REWoSprU/edit?usp=sharing)

* url: https://about.gitlab.com/events/SKO21/ (not live yet)
* **Date**: Feb 10-Feb 13
* **Location**: Sheraton Vancouver Wall Centre, 1088 Burrard St, Vancouver, BC V6Z 2R9, Canada
* Everyone must book their own travel. We will book lodging for everyone.
* You must register for the event. Registration to launch Nov 5th.

**High-Level Project Plan**
* [x]  Mid July: Obtain approval from McB and Todd to proceed
* [x]  Early Aug: Site visit
* [x]  Mid Aug: Finalize preliminary budget (target date 2019-08-13)
* [x]  Obtain budget approval
* [x]  2019-08-19: Finalize venue selection and contract
* [x]  Late Aug: Announce to Sales
* [x]  Early Sep: Core team kick off meeting
* [X]  2019-11-01: Agree on high-level project plan and key milestones
* [X]  2019-11-08: Finalize workback plan for all key workstreams/issues
* [ ]  2019-11-5: Launch SKO registration

---
### Content timeline:
* **Nov 4th** week of - issues created for all content sessions + assign DRI's
  * [**Keynote**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/104) DRI- John J ans Ashish
  * [**Keys to winning**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/107) DRI- DSomers
  * [**Partner Participation & Session**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/102) DRI- Tina
  * [**CRO Staff Level Up Panel Discussion**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/106)
  * [**Customer Speaking Session**](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/1149) DRI- Kim + Libby
  * [**Role-Based Breakout Session 1**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/108) Product / Solution Selling
  * [**Role-Based Breakout Session 2**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/109) Competitive Q&A sessions facilitated and led by Product Marketing
  * [R**ole-Based Breakout Session 3**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/110) Command of the Message and/or MEDDPICC-based role play-based training sessions facilitated by the Sales & Customer Enablement team
  * [**Role-Based Breakout Session 4**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/111) Selling skills role play-based training sessions facilitated by the Sales & Customer Enablement team
  * [**Role-Based Breakout Session 5**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/issues/112) Selling skills role play-based training sessions facilitated by the Sales & Customer Enablement team
* **Dec 1** Content outlines and abstracts due- Outline of all talks due + speaker bios
* **Jan 6th** first draft of decks due - (begin deck feedback cycle)
* **Jan 21st** next draft of sessions due, plus rehearsals start
* **Feb 3rd** Final decks due plus timed run-throughs